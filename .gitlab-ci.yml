variables:
  GIT_STRATEGY: fetch
  DOCKER_FILE: ${CI_PROJECT_DIR}/gitlab-utils/Dockerfile
  DOCKER_IMAGE_DEV: pycity_scheduling_ci


stages:
  - build
  - test
  - deploy
  - release


prepare:
  variables:
    GIT_SUBMODULE_STRATEGY: none
  stage: build
  image:
    name: gcr.io/kaniko-project/executor:debug
    entrypoint: [ "" ]
  script:
    - mkdir -p /kaniko/.docker
    - echo "{\"auths\":{\"$CI_REGISTRY\":{\"username\":\"$CI_REGISTRY_USER\",\"password\":\"$CI_REGISTRY_PASSWORD\"}}}" > /kaniko/.docker/config.json
    - /kaniko/executor --context ${CI_PROJECT_DIR} --dockerfile ${DOCKER_FILE} --target base --destination ${CI_REGISTRY_IMAGE}:${DOCKER_IMAGE_DEV}-base-image --cache=true --snapshotMode=redo --use-new-run


lint:
  stage: test
  image: ${CI_REGISTRY_IMAGE}:${DOCKER_IMAGE_DEV}-base-image
  script:
    - mkdir -p lint
    - ln -s $CI_PROJECT_DIR/LICENSE.txt $CI_PROJECT_DIR/src/LICENSE.txt
    - ln -s $CI_PROJECT_DIR/README.md $CI_PROJECT_DIR/src/README.md
    - python3.6 -m pip install --no-cache-dir $CI_PROJECT_DIR/src/.
    - pylint --rcfile=$CI_PROJECT_DIR/gitlab-utils/.pylintrc pycity_scheduling > $CI_PROJECT_DIR/gitlab-utils/pylint_report.txt || true
    - tail -n 2 $CI_PROJECT_DIR/gitlab-utils/pylint_report.txt
    - mv $CI_PROJECT_DIR/gitlab-utils/pylint_report.txt lint
  artifacts:
    name: "pylint-report"
    paths:
      - lint
    expire_in: 4 weeks
    when: always


unittest:
  stage: test
  image: ${CI_REGISTRY_IMAGE}:${DOCKER_IMAGE_DEV}-base-image
  script:
    - ln -s $CI_PROJECT_DIR/LICENSE.txt $CI_PROJECT_DIR/src/LICENSE.txt
    - ln -s $CI_PROJECT_DIR/README.md $CI_PROJECT_DIR/src/README.md
    - python3.6 -m pip install --no-cache-dir $CI_PROJECT_DIR/src/.
    - python3.6 -m unittest discover -s $CI_PROJECT_DIR/src/testing/unit_tests -v


doc:
  stage: deploy
  rules:
    - if: '$CI_COMMIT_BRANCH == "master" || $CI_COMMIT_TAG =~ /^v/'
  image: ${CI_REGISTRY_IMAGE}:${DOCKER_IMAGE_DEV}-base-image
  script:
    - mkdir -p doc
    - ln -s $CI_PROJECT_DIR/LICENSE.txt $CI_PROJECT_DIR/src/LICENSE.txt
    - ln -s $CI_PROJECT_DIR/README.md $CI_PROJECT_DIR/src/README.md
    - python3.6 -m pip install --no-cache-dir $CI_PROJECT_DIR/src/.
    - bash $CI_PROJECT_DIR/gitlab-utils/sphinx_doc/docu.sh
    - mv $CI_PROJECT_DIR/gitlab-utils/sphinx_doc/_build/html/* doc
  artifacts:
    name: "pycity_scheduling-documentation-sphinx"
    paths:
      - doc
    expire_in: 365 days
    when: always


pages:
  stage: deploy
  rules:
    - if: '$CI_COMMIT_TAG =~ /^v/'
      when: delayed
      start_in: '60 minutes'
  image: ${CI_REGISTRY_IMAGE}:${DOCKER_IMAGE_DEV}-base-image
  script:
    - mkdir -p public
    - mv $CI_PROJECT_DIR/docs/* public
  artifacts:
    paths:
      - public


pypi-release:
  stage: release
  rules:
    - if: '$CI_COMMIT_TAG =~ /^v/'
      when: delayed
      start_in: '60 minutes'
  image: ${CI_REGISTRY_IMAGE}:${DOCKER_IMAGE_DEV}-base-image
  script:
    - mkdir -p release
    - ln -s $CI_PROJECT_DIR/LICENSE.txt $CI_PROJECT_DIR/src/LICENSE.txt
    - ln -s $CI_PROJECT_DIR/README.md $CI_PROJECT_DIR/src/README.md
    - mkdir -p $CI_PROJECT_DIR/_release
    - cp -ar $CI_PROJECT_DIR/src/. $CI_PROJECT_DIR/_release/
    - python3.6 $CI_PROJECT_DIR/_release/setup.py sdist -d $CI_PROJECT_DIR/_release/dist
    - python3.6 $CI_PROJECT_DIR/_release/setup.py bdist_wheel -d $CI_PROJECT_DIR/_release/dist
    - TWINE_PASSWORD=${CI_PYPI_TOKEN} TWINE_USERNAME=__token__ python3.6 -m twine upload $CI_PROJECT_DIR/_release/dist/*
    - mv $CI_PROJECT_DIR/_release/dist/* release
  artifacts:
    name: "pycity_scheduling-release-$CI_COMMIT_TAG"
    paths:
      - release
